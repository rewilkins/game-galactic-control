#!/usr/bin/env python
import pygame
from Global_Defs import *

class Rectangle( pygame.sprite.Sprite ):
    
    def __init__(self, color = fuchsia, width = 64, height = 64, thickness = 0): # these values are defaults.  Overides can be passed in
        super( Rectangle, self ).__init__() #super allows you to use the parent class or the class you are inheriting from
        
        self.thickness = thickness
        self.width = width
        self.height = height
        self.color = color
        
        self.image = pygame.Surface( (width, height) )
        self.image.fill( color )
        self.rect = self.image.get_rect() #rect keeps track of the physical location and bounds of the object (top, bottom, right, left)
        self.hspeed = 0
        self.vspeed = 0
    
    def set_color(self, color):
        self.color = color
        self.image.fill( color )
        
    def set_alpha(self, alpha):
        self.image.set_alpha(alpha)
    
    def change_speed( self, hspeed, vspeed):
        self.hspeed += hspeed
        self.vspeed += vspeed
    
    def set_speed( self, hspeed, vspeed):
        self.hspeed = hspeed
        self.vspeed = vspeed
        
    def set_position( self, x, y ):
        self.rect.x = x
        self.rect.y = y
        
    def set_dimensions(self, width, height):
        self.width = width
        self.height = height
        
    def drag_to_set_dimensions(self, pos_start, pos_end):
        self.width = abs(pos_start[X_] - pos_end[X_])
        self.height = abs(pos_start[Y_] - pos_end[Y_])
        if(pos_end[X_] < pos_start[X_]):  # if x-end is less then x-start, offset the rect by the width of the rect
            self.set_position(pos_end[X_], self.rect.y)
        if(pos_end[Y_] < pos_start[Y_]):  # if y-end is less then y-start, offset the rect by the height of the rect
            self.set_position(self.rect.x, pos_end[Y_])
        if(pos_end[X_] > pos_start[X_]):  # if x-end is greater then x-start, undo the offset from the first if func
            self.set_position(pos_start[X_], self.rect.y)
        if(pos_end[Y_] > pos_start[Y_]):  # if y-end is greater then y-start, undo the offset from the second if func
            self.set_position(self.rect.x, pos_start[Y_])
        
    def set_image( self, filename = None ):
        if( filename != None ):
            self.image = pygame.image.load( filename )
            # self.rect = self.image.get_rect()
            
    def update( self ):
        self.rect.x += self.hspeed
        self.rect.y += self.vspeed
        
    def display(self, window):
        # pygame.draw.rect(screen, color, (x,y,width,height), thickness)
        pygame.draw.rect(window, self.color, (self.rect.x, self.rect.y, self.width, self.height), self.thickness)
        
    def collisionDetection(self, rect2):
        collision = False
        
        top1 = self.rect.y
        bottom1 = self.rect.y + self.height
        left1 = self.rect.x
        right1 = self.rect.x + self.width
        
        top2 = rect2.rect.y
        bottom2 = rect2.rect.y + rect2.height
        right2 = rect2.rect.x
        left2 = rect2.rect.x + rect2.width
        
        # print str(bottom1) +" >= "+ str(top2) +" and "+ str(top1) +" <= "+ str(bottom2) +" and "+ str(right1) +" >= "+ str(right2) +" and "+ str(left1) +" <= "+ str(left2)
        if(bottom1 >= top2 and top1 <= bottom2 and right1 >= right2 and left1 <= left2):
            collision = True
        return collision