#!/usr/bin/env python
from Global_Defs import *

### sudo explanation ###
#(
#if num of bases < 5:
#   => go find gray bases
#   => if there is no gray bases:
#       => go capture player bases
#)

#else:

#(
#if num of bases >= 5:
#   => if num of bases >= player num of bases:
#       => go capture gray base
#       => if there is no gray bases:
#           => go capture player bases
#   => if num of bases < player num of bases:
#       => go find gray bases
#       => if there is no gray bases:
#           => go capture player bases
#)
### end sudo ###

def scan_for_base(color, enemy_ship, background_base_group):
    # scans the bases and returns the base number that is the closes to the ship
    shortest_dist = -1
    base_number = -1
    counter = 0
    for b in background_base_group:
        if (b.color == color):
            dist = b.getDist(enemy_ship.x, enemy_ship.y)
            if(shortest_dist == -1):
                shortest_dist = dist
                base_number = counter
            elif(dist < shortest_dist):
                shortest_dist = dist
                base_number = counter
        counter = counter + 1
    return base_number

def run_AI(enemy_ship, background_base_group, player_color, enemy_color):
    player_count = 0
    enemy_count = 0
    nutral_count = 0
    base_target = 0
    comp_safe_perception = 7
    
    # get base counts
    for i in background_base_group:
        if(i.color == player_color):
            player_count = player_count + 1
        if(i.color == enemy_color):
            enemy_count = enemy_count + 1
        if(i.color == whitesmoke):
            nutral_count = nutral_count + 1
     
    # start AI algorithm       
    if (enemy_count < 5): # if num of bases < 5: (if start of game or almost eliminated)
        if(nutral_count > comp_safe_perception): # if the chances of getting a gray base are still good
            base_target = scan_for_base(whitesmoke, enemy_ship, background_base_group) # go find gray bases
        else: # if the chances of getting a gray base are not good
            base_target = scan_for_base(player_color, enemy_ship, background_base_group) # go capture player bases
    
    else: # if num of bases >= 5: (if not start of game and have a healthy base control)
        if(enemy_count >= player_count): # if doing better then the human player:
            if(nutral_count > 0): # if there still are gray bases to capture
                base_target = scan_for_base(whitesmoke, enemy_ship, background_base_group) # go find gray bases
            else: # if there are no other gray bases to capture
                base_target = scan_for_base(player_color, enemy_ship, background_base_group) # go capture player bases
        else: # if losing to the human player:
            if(nutral_count > comp_safe_perception): # if the chances of getting a gray base are still good
                base_target = scan_for_base(whitesmoke, enemy_ship, background_base_group) # go find gray bases
            else: # if the chances of getting a gray base are not good
                base_target = scan_for_base(player_color, enemy_ship, background_base_group) # go capture player bases

    return base_target   
            

