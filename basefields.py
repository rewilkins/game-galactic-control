#!/usr/bin/env python
# -*- coding: utf-8 -*-

# the base of this code was taken from:
# http://codeboje.de/starfields-and-galaxies-python/
# It has been added to and modified to fit the
# purposes for this project

import random
from math import sin, cos,pi
from Circle import *
import pygame
from Global_Defs import *

class Basefields():
  def __init__(self, width, height):
    self.width=width
    self.height=height
    random.seed()
    self.background_base_group = []
    self.center = (0, 0)

  def getGalaxy(self):
      return self.background_base_group

  def createElipticStarfield(self, amount, colors, center, radius, turn=0):
      lencol=len(colors)
      rx,ry=radius
      x,y=center

      for star in range(0,int(amount)):
          degree=random.randint(0,360)
          
          degree= 2.0 * degree * pi / 360.0
          #(sin(degree)*rx/ypos=cos(degree)*ry) would form the elipse
          #since we need to draw inside with the density increaing near the center we must include 
          #some factor (0..1) 
          insideFactor=float(random.randint(0,10000))/10000
          insideFactor=insideFactor*insideFactor
          xpos=sin(degree)*round(insideFactor*rx)
          ypos=cos(degree)*round(insideFactor*ry)
          if turn!=0:
              xptemp=cos(turn)*xpos+sin(turn)*ypos
              yptemp=-sin(turn)*xpos+cos(turn)*ypos
              xpos=xptemp
              ypos=yptemp 
          self.load(x+xpos,y+ypos,colors[random.randint(0,lencol-1)])

  def createSpiralGalaxy(self, colors, center, size, turn=0, deg=0, dynsizefactor=50, sPCFactor=8): #dynsizefactor = overal galaxy scale, sPCFactor = how compacte the stars in the arms are
      sx,sy=size
      sx=2.0*sx*pi/360.0
      sy=2.0*sy*pi/360.0
      x, y = center
      self.center = center
      swap=True
      xp1=round(deg/pi*sx/1.7)*dynsizefactor
      yp1=round(deg/pi*sy/1.7)*dynsizefactor #factors for dynamic sizing

      self.createElipticStarfield(50,darkgray,center,(xp1*1.5,yp1*1.5),turn) # center eliptical cluster only
      #this was the central cloud
      #now for the smaller ones in the spiral arms
      mulStarAmount=(xp1+yp1)/sPCFactor #factor for amount of stars per cloud 
      n=0.0
      while n<=deg:
          swap = not swap
          xpos=(cos(n)*((n*sx))*dynsizefactor)
          ypos=(sin(n)*((n*sy))*dynsizefactor)
          xp1=cos(turn)*xpos  + sin(turn)*ypos 
          yp1=-sin(turn)*xpos + cos(turn)*ypos
          sizetemp=2+(mulStarAmount*n)
          if swap:
              self.createElipticStarfield(1,darkgray,(x+xp1,y+yp1),(sizetemp,sizetemp), turn)
          else:
              self.createElipticStarfield(1,darkgray,(x-xp1,y-yp1),(sizetemp,sizetemp), turn)
          angle=random.randint(0,4)+1
          n+= 2.0* angle *pi / 360.0

      numOfArms = 4
      if(numOfArms == 4):
        turn = 135.0 * 2 * math.pi / 360.0 #calculate in rad
        n=0.0
        while n<=deg:
            swap = not swap
            xpos=(cos(n)*((n*sx))*dynsizefactor)
            ypos=(sin(n)*((n*sy))*dynsizefactor)
            xp1=cos(turn)*xpos  + sin(turn)*ypos 
            yp1=-sin(turn)*xpos + cos(turn)*ypos
            sizetemp=2+(mulStarAmount*n)
            if swap:
                self.createElipticStarfield(1,colors,(x+xp1,y+yp1),(sizetemp,sizetemp), turn)
            else:
                self.createElipticStarfield(1,colors,(x-xp1,y-yp1),(sizetemp,sizetemp), turn)
            angle=random.randint(0,4)+1
            n+= 2.0* angle *pi / 360.0

  # overwrite this for your own graphics library
  def load(self,x,y,color):
    collision = False
    closeToBlackHole = False
    star = Circle((int(x),int(y)), 15, whitesmoke, 3)

    for i in self.background_base_group:      
      collision = False
      closeToBlackHole = False
      safeDist = 40  # star systems cant be any closser to black hole then this
      
      # collision = star.collisionDetection(i)
      dist = math.sqrt((math.pow(star.x - i.x, 2)) + (math.pow(star.y - i.y, 2)))
      if(dist <= safeDist*2):
        collision = True
        
      dist = math.sqrt((math.pow(star.x - self.center[X_], 2)) + (math.pow(star.y - self.center[Y_], 2)))
      if(dist <= safeDist):
        closeToBlackHole = True
        
      if(collision == True or closeToBlackHole == True):
        break

    if(collision == False and closeToBlackHole == False):
      self.background_base_group.append(star)

