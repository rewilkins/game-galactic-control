#!/usr/bin/env python
import pygame
from Global_Defs import *
import math

class Circle( pygame.sprite.Sprite ):
    
    def __init__(self, (x, y) = (0, 0), r = 64, color = fuchsia, thickness = 1 ): # these values are defaults.  Overides can be passed in
        self.x = x
        self.y = y
        self.size = r
        self.color = color
        self.thickness = thickness
        self.hspeed = 0
        self.vspeed = 0
        
    def set_color(self, color):
        self.color = color
            
    def change_speed( self, hspeed, vspeed):
        self.hspeed += hspeed
        self.vspeed += vspeed
        
    def set_position( self, x, y ):
        self.x = x
        self.y = y
        
    def set_image( self, filename = None ):
        if( filename != None ):
            self.image = pygame.image.load( filename )
            
    def update( self ):
        self.x += self.hspeed
        self.y += self.vspeed
        
    def display(self, window):
        pygame.draw.circle(window, self.color, (self.x, self.y), self.size, self.thickness)
        
    def collisionDetection(self, circ2):
        dist = math.sqrt((math.pow(self.x - circ2.x, 2)) + (math.pow(self.y - circ2.y, 2)))
        totalSize = self.size + circ2.size
        
        if(dist <= totalSize):
            return True
        return False
    
    def getDist(self, locX, locY):
        return math.sqrt((math.pow(self.x - locX, 2)) + (math.pow(self.y - locY, 2)))
    
