#!/usr/bin/env python

import pygame
from Global_Defs import *
from Map import *
from Rectangle import *


    
def Instruction_loop(window):
    
    fpsClock = pygame.time.Clock()

    myfont = pygame.font.SysFont("monospace", 45, bold=True)
    fps_font = pygame.font.SysFont("monospace", 24, bold=True)

    label3_selected = True
    label3_area = Rectangle(fuchsia, 200, 90, 2)
    label3_area.set_position(400 - 45, 400 - 22)
    
    instruction_group = pygame.sprite.Group()
    instruction_sheet = Rectangle(931, 675, 0)
    instruction_sheet.set_image("./Instructions_Sheet.gif")
    instruction_sheet.set_position(600, 250)
    instruction_group.add( instruction_sheet )
    
    mouse_position = Rectangle(fuchsia, 2, 2)
  
#       Game loop
    while(True):
#           variables:
        deltaTime = fpsClock.tick(maxFPS)
            
#           Event loop
        for event in pygame.event.get():
            keyState = pygame.key.get_pressed()
            if (event.type == pygame.QUIT):
                return
            
            if( event.type == pygame.MOUSEMOTION):
                if(mouse_position.collisionDetection(label3_area)):
                    label3_selected = True
                else:
                    label3_selected = False
                    
            if (event.type == pygame.MOUSEBUTTONDOWN and event.button == LEFT_):

                if(label3_selected == True):
                    return

#       Update Phase
            
        label_fps = fps_font.render(str(int(fpsClock.get_fps())), 1, red)

        mouse_pos = pygame.mouse.get_pos()
        mouse_position.set_position(mouse_pos[X_], mouse_pos[Y_])
        
        if(label3_selected == False):
            label3 = myfont.render("Back", 1, yellowgreen)
        else:
            label3 = myfont.render("Back", 1, orange)


        instruc1 = myfont.render("Back", 1, yellowgreen)


#       Draw Phase
        
        window.fill( black )
        
        window.blit(label_fps, (400, 200))
        
        window.blit(label3, (400, 400))
        
        instruction_group.draw( window )

        # mouse_position.display(window)
        # label3_area.display(window)
        
        pygame.display.update() # update("empty") = means that it will update everything
            
            
            