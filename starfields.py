#!/usr/bin/env python
# -*- coding: utf-8 -*-

# the base of this code was taken from:
# http://codeboje.de/starfields-and-galaxies-python/
# It has been added to and modified to fit the
# purposes for this project

import random
from math import sin, cos, pi
from Rectangle import *
from Circle import *
from Global_Defs import *
import pygame

class Starfields():
  def __init__(self, width, height):
    self.width=width
    self.height=height
    random.seed()
    self.background_galaxy_group = []
    self.load_progress = 0.0
    self.myfont = pygame.font.SysFont("monospace", 45, bold=True)
    self.loading_galaxy = Rectangle(1401, 788, 0)
    self.galaxy_group = pygame.sprite.Group()
    self.loading_inited = False
    self.counter = 0

  def load_screen(self, window):
    if(self.loading_inited == False):
      self.loading_inited = True
      
      self.loading_galaxy.set_image("loading_galaxy.gif")
      self.loading_galaxy.set_position((map_center_x - (1401/2)) + window_width, map_center_y - (788/2))
      self.galaxy_group.add( self.loading_galaxy )
    
    self.load_progress = self.load_progress + 1
    load_percent = (self.load_progress / 4000) * 100
    
    percent_label = self.myfont.render(str(int(load_percent)) + "%", 1, yellowgreen)
    
    if(load_percent <= 100):
      # print "in"
      self.counter = self.counter + 1
      if(self.counter%2 == 0):  # this slows down the galaxy side scrolling speed to slower then 1
        self.loading_galaxy.set_speed( -1, 0 )
      else:
        self.loading_galaxy.set_speed( 0, 0 )
      if(self.loading_galaxy.rect.x == 260):  # if galaxy reaches the center of the screen stop its scrolling
        self.loading_galaxy.set_speed( 0, 0 )  
      
      window.fill( black )
      self.loading_galaxy.update()
      # self.loading_galaxy.display(window)
      self.galaxy_group.draw( window )
      window.blit(percent_label, map_center)
      pygame.display.update()

  def getGalaxy(self):
      return self.background_galaxy_group

  def createElipticStarfield(self, window, amount, colors, center, radius, turn=0):
      lencol=len(colors)
      rx,ry=radius
      x,y=center

      for star in range(0,int(amount)):
          degree=random.randint(0,360)
          
          degree= 2.0 * degree * pi / 360.0
          #(sin(degree)*rx/ypos=cos(degree)*ry) would form the elipse
          #since we need to draw inside with the density increaing near the center we must include 
          #some factor (0..1) 
          insideFactor=float(random.randint(0,10000))/10000
          insideFactor=insideFactor*insideFactor
          xpos=sin(degree)*round(insideFactor*rx)
          ypos=cos(degree)*round(insideFactor*ry)
          if turn!=0:
              xptemp=cos(turn)*xpos+sin(turn)*ypos
              yptemp=-sin(turn)*xpos+cos(turn)*ypos
              xpos=xptemp
              ypos=yptemp 
          self.load(window, x+xpos,y+ypos,colors[random.randint(0,lencol-1)])

  def createSpiralGalaxy(self,window, colors, center, size, turn=0, deg=0, dynsizefactor=50, sPCFactor=8): #dynsizefactor = overal galaxy scale, sPCFactor = how compacte the stars in the arms are
      sx,sy=size
      sx=(2.0*sx*pi/360.0)/2
      sy=(2.0*sy*pi/360.0)/2
      x,y=center
      swap=True
      xp1=round(deg/pi*sx/1.7)*dynsizefactor
      yp1=round(deg/pi*sy/1.7)*dynsizefactor #factors for dynamic sizing

      self.createElipticStarfield(window, 5*(xp1*2.0+yp1*2.0),colors,center,(xp1*1.0,yp1*1.0),turn) # center eliptical cluster only
      #this was the central cloud
      #now for the smaller ones in the spiral arms
      mulStarAmount=(xp1+yp1)/sPCFactor #factor for amount of stars per cloud 
      n=0.0
      while n<=deg:
          swap = not swap
          xpos=(cos(n)*((n*sx))*dynsizefactor)
          ypos=(sin(n)*((n*sy))*dynsizefactor)
          xp1=cos(turn)*xpos  + sin(turn)*ypos 
          yp1=-sin(turn)*xpos + cos(turn)*ypos
          sizetemp=2+(mulStarAmount*n)
          if swap:
              self.createElipticStarfield(window, int(sizetemp/2),colors,(x+xp1,y+yp1),(sizetemp,sizetemp), turn)
          else:
              self.createElipticStarfield(window, int(sizetemp/2),colors,(x-xp1,y-yp1),(sizetemp,sizetemp), turn)
          angle=random.randint(0,4)+1
          n+= 2.0* angle *pi / 360.0
          
      
      numOfArms = 4
      if(numOfArms == 4):
        turn = 135.0 * 2 * pi / 360.0 #calculate in rad
        n=0.0
        while n<=deg:
            swap = not swap
            xpos=(cos(n)*((n*sx))*dynsizefactor)
            ypos=(sin(n)*((n*sy))*dynsizefactor)
            xp1=cos(turn)*xpos  + sin(turn)*ypos 
            yp1=-sin(turn)*xpos + cos(turn)*ypos
            sizetemp=2+(mulStarAmount*n)
            if swap:
                self.createElipticStarfield(window, int(sizetemp/2),colors,(x+xp1,y+yp1),(sizetemp,sizetemp), turn)
            else:
                self.createElipticStarfield(window, int(sizetemp/2),colors,(x-xp1,y-yp1),(sizetemp,sizetemp), turn)
            angle=random.randint(0,4)+1
            n+= 2.0* angle *pi / 360.0

  # overwrite this for your own graphics library
  def load(self, window, x, y, color):
    
    collision = False

    randSize = random.randint(2,3)
    star = Rectangle(color, randSize, randSize)
    star.set_position(int(x), int(y))

    # self.background_galaxy_group.append(star) # comment this out and uncomment the rest to optimize
  
    for i in self.background_galaxy_group:
      collision = star.collisionDetection(i)
      if(collision == True): break
    
    if(collision == False):
      self.background_galaxy_group.append(star)
      self.load_screen(window)

    
