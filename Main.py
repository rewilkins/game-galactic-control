#!/usr/bin/env python

#requirments:
#    python 2.7
#    pyGame 1.9.1

# ambient music taken from an Orbiter Flight sim addon
# http://orbit.medphys.ucl.ac.uk/

import pygame
from Global_Defs import *
from Map import *
from Instructions import *
from Rectangle import *

class Menu(object):
    
    def transition_to_load(self, trans_delay, fpsClock, tran_time = 0, player_color = white, enemy_color = black):
        
        # print(str(trans_delay) + " --- " + str(tran_time * fpsClock.get_fps()))
        if(trans_delay > tran_time * fpsClock.get_fps()):
            Map().main(window, player_color, enemy_color)
            return True
        return False 
    
    def Main_Menu(self, window):
        
        self.bg_sound = pygame.mixer.Sound( "calm-spaces.ogg" )
        self.bg_sound.set_volume(0.25)
        self.bg_sound.play(loops = -1)
        
        pygame.display.set_caption( "Final Project")
        fpsClock = pygame.time.Clock()

        myfont = pygame.font.SysFont("monospace", 45, bold=True)
        fps_font = pygame.font.SysFont("monospace", 24, bold=True)
        label1_selected = False
        label2_selected = False
        label3_selected = False

        mouse_position = Rectangle(fuchsia, 2, 2)
        label1_area = Rectangle(fuchsia, 400, 90, 2)
        label2_area = Rectangle(fuchsia, 200, 90, 2)
        label3_area = Rectangle(fuchsia, 200, 90, 2)
        
        label1_area.set_position(400 - 45, 400 - 22)
        label2_area.set_position(400 - 45, 400 + 90 - 22)
        label3_area.set_position(400 - 45, 400 + 90 + 90 - 22)
        
        pick_a_color = False
        color_blue = Rectangle(darkgray, 40, 40, 2)
        color_green = Rectangle(darkgray, 40, 40, 2)
        color_orange = Rectangle(darkgray, 40, 40, 2)
        color_purple = Rectangle(darkgray, 40, 40, 2)
        color_red = Rectangle(darkgray, 40, 40, 2)
        player_color = white
        
        color_blue_selected = False
        color_green_selected = False
        color_red_selected = False
        color_purple_selected = False
        color_orange_selected = False
        
        trans_to_load = False
        trans_delay = 0
        
        start_time = 0
        prevent_double_enter_sound = True
        
#       init background
        background_bg01_group = pygame.sprite.Group()
        for i in range(1, star_density):
            star_color = random.randrange(1, 100)

            if(star_color > 0 and star_color <= line1):
                background_bg01 = Rectangle( white, 2, 2 )
            if(star_color > line1 and star_color <= line2):
                background_bg01 = Rectangle( yellow, 2, 2 )
            if(star_color > line2 and star_color <= line3):
                background_bg01 = Rectangle( royalblue, 3, 3 )
            if(star_color > line3 and star_color <= line4):
                background_bg01 = Rectangle( orangered, 4, 4 )
            background_bg01.set_position(random.randrange(render_zone_left, render_zone_right), random.randrange(render_zone_top, render_zone_bottom))
            background_bg01_group.add( background_bg01 )
        
        background_bg02_group = pygame.sprite.Group()
        for i in range(1, star_density):
            star_color = random.randrange(1, 100)
            background_bg02 = Rectangle( white, 2, 2 )

            if(star_color > 0 and star_color <= line1):
                background_bg02 = Rectangle( white, 2, 2 )
            if(star_color > line1 and star_color <= line2):
                background_bg02 = Rectangle( yellow, 2, 2 )
            if(star_color > line2 and star_color <= line3):
                background_bg02 = Rectangle( royalblue, 3, 3 )
            if(star_color > line3 and star_color <= line4):
                background_bg02 = Rectangle( orangered, 4, 4 )

            background_bg02.set_position(random.randrange(render_zone_left, render_zone_right), random.randrange(render_zone_top, render_zone_bottom))
            background_bg02_group.add( background_bg02 )
        
        background_bg03_group = pygame.sprite.Group()
        for i in range(1, star_density):
            star_color = random.randrange(1, 100)
            background_bg03 = Rectangle( white, 2, 2 )

            if(star_color > 0 and star_color <= line1):
                background_bg03 = Rectangle( white, 2, 2 )
            if(star_color > line1 and star_color <= line2):
                background_bg03 = Rectangle( yellow, 2, 2 )
            if(star_color > line2 and star_color <= line3):
                background_bg03 = Rectangle( royalblue, 3, 3 )
            if(star_color > line3 and star_color <= line4):
                background_bg03 = Rectangle( orangered, 4, 4 )

            background_bg03.set_position(random.randrange(render_zone_left, render_zone_right), random.randrange(render_zone_top, render_zone_bottom))
            background_bg03_group.add( background_bg03 )
        
        background_bg04_group = pygame.sprite.Group()
        for i in range(1, star_density):
            star_color = random.randrange(1, 100)
            background_bg04 = Rectangle( white, 2, 2 )

            if(star_color > 0 and star_color <= line1):
                background_bg04 = Rectangle( white, 2, 2 )
            if(star_color > line1 and star_color <= line2):
                background_bg04 = Rectangle( yellow, 2, 2 )
            if(star_color > line2 and star_color <= line3):
                background_bg04 = Rectangle( royalblue, 3, 3 )
            if(star_color > line3 and star_color <= line4):
                background_bg04 = Rectangle( orangered, 4, 4 )

            background_bg04.set_position(random.randrange(render_zone_left, render_zone_right), random.randrange(render_zone_top, render_zone_bottom))
            background_bg04_group.add( background_bg04 )
        
        background_bg05_group = pygame.sprite.Group()
        for i in range(1, star_density):
            star_color = random.randrange(1, 100)
            background_bg05 = Rectangle( white, 2, 2 )

            if(star_color > 0 and star_color <= line1):
                background_bg05 = Rectangle( white, 2, 2 )
            if(star_color > line1 and star_color <= line2):
                background_bg05 = Rectangle( yellow, 2, 2 )
            if(star_color > line2 and star_color <= line3):
                background_bg05 = Rectangle( royalblue, 3, 3 )
            if(star_color > line3 and star_color <= line4):
                background_bg05 = Rectangle( orangered, 4, 4 )

            background_bg05.set_position(random.randrange(render_zone_left, render_zone_right), random.randrange(render_zone_top, render_zone_bottom))
            background_bg05_group.add( background_bg05 )
        
        background_bg06_group = pygame.sprite.Group()
        for i in range(1, star_density):
            star_color = random.randrange(1, 100)
            background_bg06 = Rectangle( white, 2, 2 )

            if(star_color > 0 and star_color <= line1):
                background_bg06 = Rectangle( white, 2, 2 )
            if(star_color > line1 and star_color <= line2):
                background_bg06 = Rectangle( yellow, 2, 2 )
            if(star_color > line2 and star_color <= line3):
                background_bg06 = Rectangle( royalblue, 3, 3 )
            if(star_color > line3 and star_color <= line4):
                background_bg06 = Rectangle( orangered, 4, 4 )

            background_bg06.set_position(random.randrange(render_zone_left, render_zone_right), random.randrange(render_zone_top, render_zone_bottom))
            background_bg06_group.add( background_bg06 )

        for i in background_bg06_group:
            i.change_speed( -6, 0 )
        for i in background_bg05_group:
            i.change_speed( -5, 0 )
        for i in background_bg04_group:
            i.change_speed( -4, 0 )
        for i in background_bg03_group:
            i.change_speed( -3, 0 )
        for i in background_bg02_group:
            i.change_speed( -2, 0 )
        for i in background_bg01_group:
            i.change_speed( -1, 0 )
                
#       Game loop
        while(True):
#           variables:
            deltaTime = fpsClock.tick(maxFPS)
            
            if(trans_to_load == False):
                window.fill( black )
                
            
            
#           Event loop
            for event in pygame.event.get():
                keyState = pygame.key.get_pressed()
                if (event.type == pygame.QUIT):
                    return
                
                if( event.type == pygame.MOUSEMOTION):
                    # print "1"
                    if(mouse_position.collisionDetection(label1_area)):
                        label1_selected = True
                    else:
                        label1_selected = False
                    # print "2"
                    if(mouse_position.collisionDetection(label2_area)):
                        label2_selected = True
                    else:
                        label2_selected = False
                    # print "3"
                    if(mouse_position.collisionDetection(label3_area)):
                        label3_selected = True
                    else:
                        label3_selected = False
                        
                        
                    if(mouse_position.collisionDetection(color_blue)):
                        color_blue_selected = True
                    else:
                        color_blue_selected = False
                    if(mouse_position.collisionDetection(color_green)):
                        color_green_selected = True
                    else:
                        color_green_selected = False
                    if(mouse_position.collisionDetection(color_orange)):
                        color_orange_selected = True
                    else:
                        color_orange_selected = False
                    if(mouse_position.collisionDetection(color_purple)):
                        color_purple_selected = True
                    else:
                        color_purple_selected = False
                    if(mouse_position.collisionDetection(color_red)):
                        color_red_selected = True
                    else:
                        color_red_selected = False
                
                if (event.type == pygame.MOUSEBUTTONDOWN and event.button == LEFT_):
                    if(pick_a_color == False):
                        if(label1_selected == True):
                            Instruction_loop(window)
                        elif(label2_selected == True):
                            pick_a_color = True
                        elif(label3_selected == True):
                            return
                    if(pick_a_color == True):
                        if(label3_selected == True):
                            pick_a_color = False
                        if(color_blue_selected == True):
                            pick_a_color = False
                            trans_to_load = True
                            prevent_double_enter_sound = False
                            player_color = blue
                            enemy_color = green
                        if(color_green_selected == True):
                            pick_a_color = False
                            trans_to_load = True
                            prevent_double_enter_sound = False
                            player_color = green
                            enemy_color = orange
                        if(color_orange_selected == True):
                            pick_a_color = False
                            trans_to_load = True
                            prevent_double_enter_sound = False
                            player_color = orange
                            enemy_color = purple
                        if(color_purple_selected == True):
                            pick_a_color = False
                            trans_to_load = True
                            prevent_double_enter_sound = False
                            player_color = purple
                            enemy_color = red
                        if(color_red_selected == True):
                            pick_a_color = False
                            trans_to_load = True
                            prevent_double_enter_sound = False
                            player_color = red
                            enemy_color = blue
                        if(trans_to_load == True and prevent_double_enter_sound == False):
                            start_time = pygame.time.get_ticks()
            
            
#           wrap stars around the screen when they go offscreen
            for i in background_bg06_group:
                if(i.rect.x < render_zone_left):
                    i.set_position( i.rect.x + render_zone_right, i.rect.y )
            for i in background_bg05_group:
                if(i.rect.x < render_zone_left):
                    i.set_position( i.rect.x + render_zone_right, i.rect.y )
            for i in background_bg04_group:
                if(i.rect.x < render_zone_left):
                    i.set_position( i.rect.x + render_zone_right, i.rect.y )
            for i in background_bg03_group:
                if(i.rect.x < render_zone_left):
                    i.set_position( i.rect.x + render_zone_right, i.rect.y )
            for i in background_bg02_group:
                if(i.rect.x < render_zone_left):
                    i.set_position( i.rect.x + render_zone_right, i.rect.y )
            for i in background_bg01_group:
                if(i.rect.x < render_zone_left):
                    i.set_position( i.rect.x + render_zone_right, i.rect.y )
            
            
#           Update Phase

            if(trans_to_load == True):
                tran_time = 1.75
                
                now = pygame.time.get_ticks()
                if (now - start_time >= 700 and prevent_double_enter_sound == False):
                    start_time = 0
                    prevent_double_enter_sound = True
                    self.bg_sound = pygame.mixer.Sound( "enter-jump.ogg" )
                    # self.bg_sound.set_volume(0.25)
                    self.bg_sound.play(loops = 0)
                    # pygame.time.delay(1000)
                
                trans_delay = trans_delay + 1
                if(trans_delay > tran_time * fpsClock.get_fps()):
                    window.fill( white )
                    pygame.display.update()
                    pygame.time.delay(50)
                    
                end = self.transition_to_load(trans_delay, fpsClock, tran_time, player_color, enemy_color)
                if(end == True):
                    trans_delay = 0
                    trans_to_load = False
                    label3_selected = True

            label_fps = fps_font.render(str(int(fpsClock.get_fps())), 1, red)

            mouse_pos = pygame.mouse.get_pos()
            mouse_position.set_position(mouse_pos[X_], mouse_pos[Y_])
            
            if(trans_to_load == False):
                if(pick_a_color == False):
                    if(label1_selected == False):
                        label1 = myfont.render("Instructions", 1, yellowgreen)
                    else:
                        label1 = myfont.render("Instructions", 1, orange)
                    if(label2_selected == False):
                        label2 = myfont.render("Play", 1, yellowgreen)
                    else:
                        label2 = myfont.render("Play", 1, orange)
                    if(label3_selected == False):
                        label3 = myfont.render("Quit", 1, yellowgreen)
                    else:
                        label3 = myfont.render("Quit", 1, orange)
    
                if(pick_a_color == True):
                    label1 = myfont.render("Pick Your Color:", 1, yellowgreen)
                    if(label3_selected == False):
                        label3 = myfont.render("Back", 1, yellowgreen)
                    else:
                        label3 = myfont.render("Back", 1, orange)
            
                    if(color_blue_selected == False):
                        color_blue = Rectangle(blue, 40, 40, 2)
                    else:
                        color_blue = Rectangle(blue, 40, 40, 0)
                    if(color_green_selected == False):
                        color_green = Rectangle(green, 40, 40, 2)
                    else:
                        color_green = Rectangle(green, 40, 40, 0)
                    if(color_red_selected == False):
                        color_red = Rectangle(red, 40, 40, 2)
                    else:
                        color_red = Rectangle(red, 40, 40, 0)
                    if(color_purple_selected == False):
                        color_purple = Rectangle(purple, 40, 40, 2)
                    else:
                        color_purple = Rectangle(purple, 40, 40, 0)
                    if(color_orange_selected == False):
                        color_orange = Rectangle(orange, 40, 40, 2)
                    else:
                        color_orange = Rectangle(orange, 40, 40, 0)
                
                color_blue.set_position(400 - 45 + 200, 400 + 120 - 22)
                color_green.set_position(400 - 45 + 200 + 80, 400 + 120 - 22)
                color_red.set_position(400 - 45 + 200 + 80 + 80, 400 + 120 - 22)
                color_purple.set_position(400 - 45 + 200 + 80 + 80 + 80, 400 + 120 - 22)
                color_orange.set_position(400 - 45 + 200 + 80 + 80 + 80 + 80, 400 + 120 - 22)
        
        
        
            background_bg06_group.update()
            background_bg05_group.update()
            background_bg04_group.update()
            background_bg03_group.update()
            background_bg02_group.update()
            background_bg01_group.update()


#           Draw Phase
            background_bg01_group.draw( window )
            background_bg02_group.draw( window )
            background_bg03_group.draw( window )
            background_bg04_group.draw( window )
            background_bg05_group.draw( window )
            background_bg06_group.draw( window )
            
            window.blit(label_fps, (400,200))
            
            if(trans_to_load == False):
                window.blit(label1, (400,400))
                if(pick_a_color == False): window.blit(label2, (400,400 + 90))
                window.blit(label3, (400,400 + 90 + 90))
                
                if(pick_a_color == True):
                    color_blue.display(window)
                    color_green.display(window)
                    color_red.display(window)
                    color_purple.display(window)
                    color_orange.display(window)
            
            # mouse_position.display(window)
            # label1_area.display(window)
            # label2_area.display(window)
            # label3_area.display(window)
            
            pygame.display.update() # update("empty") = means that it will update everything
            
if __name__ == '__main__':
    pygame.init()
    window = pygame.display.set_mode(window_size, pygame.FULLSCREEN)
    os.chdir("./")
    Menu().Main_Menu(window)
    pygame.quit()