#!/usr/bin/env python

import pygame
import math
from Global_Defs import *

#click to move functionality taken from: http://stackoverflow.com/questions/16288905/make-a-sprite-move-to-the-mouse-click-position-step-by-step

# some simple vector helper functions, stolen from http://stackoverflow.com/a/4114962/142637
def magnitude(v):
    return math.sqrt(sum(v[i]*v[i] for i in range(len(v))))

def add(u, v):
    return [ u[i]+v[i] for i in range(len(u)) ]

def sub(u, v):
    return [ u[i]-v[i] for i in range(len(u)) ]    

def dot(u, v):
    return sum(u[i]*v[i] for i in range(len(u)))

def normalize(v):
    vmag = magnitude(v)
    return [ v[i]/vmag  for i in range(len(v)) ]

class Ship(object):
    def __init__(self, (x, y) = (0, 0), r = 64, color = fuchsia, thickness = 1 ):
        self.x, self.y = (x, y)
        self.size = r
        self.color = color
        self.thickness = thickness
        self.hspeed = 0
        self.vspeed = 0
        self.moving = False
        self.moved = False
        
        self.selected = False
        self.dist = 0
        
        self.set_target((x, y))
        
        self.attach_power = 1
        self.defense = 1
        self.speed = 0.5




    def set_color(self, color):
        self.color = color
            
    def change_speed( self, hspeed, vspeed):
        self.hspeed += hspeed
        self.vspeed += vspeed
        
    def set_position( self, x, y ):
        self.x = x
        self.y = y
        if(self.moving == False):
            self.t_x = x
            self.t_y = y

    def update( self):
        self.x += self.hspeed
        self.y += self.vspeed
        self.t_x += self.hspeed
        self.t_y += self.vspeed
        
        self.moved = False
        
        # print "-----------------------"
        # print "ship #: " + str(i)
        # print str(self.int_pos)
        # print str(self.int_target)
        # print "-----------------------"
        
        # if we won't move, don't calculate new vectors
        if self.int_pos == self.int_target:
            self.moving = False
            return
        
        target_vector = sub(self.target, self.pos) 

        # a threshold to stop moving if the distance is to small.
        # it prevents a 'flickering' between two points
        self.dist = magnitude(target_vector)
        if magnitude(target_vector) < 2: 
            return

        # apply the ship's speed to the vector
        move_vector = [c * self.speed for c in normalize(target_vector)]

        # update position
        self.x, self.y = add(self.pos, move_vector)
        
    def display(self, window):
        # draw ship
        pygame.draw.circle(window, self.color, self.int_pos, self.size, self.thickness)
        if(self.selected == True):
            # draw selected ship indicator (green ring)
            pygame.draw.circle(window, limegreen, self.int_pos, self.size*2, 1)

    def collisionDetection(self, circ2):
        dist = math.sqrt((math.pow(self.x - circ2.x, 2)) + (math.pow(self.y - circ2.y, 2)))
        totalSize = self.size + circ2.size
        
        if(dist <= totalSize):
            return True
        return False
    
    def collisionDetection_rect(self, rect_in):
        collision = False
        
        top2 = rect_in.rect.y
        bottom2 = rect_in.rect.y + rect_in.height
        left2 = rect_in.rect.x
        right2 = rect_in.rect.x + rect_in.width
        
        # print str(self.y) +" >= "+ str(top2) +" and "+ str(self.y) +" <= "+ str(bottom2) +" and "+ str(self.x) +" <= "+ str(right2) +" and "+ str(self.x) +" >= "+ str(left2)
        if(self.y >= top2 and self.y <= bottom2 and self.x <= right2 and self.x >= left2):
            collision = True
        return collision
    
    def getDist(self, locX, locY):
        return math.sqrt((math.pow(self.x - locX, 2)) + (math.pow(self.y - locY, 2)))


    @property
    def pos(self):
        return self.x, self.y

    # for drawing, we need the position as tuple of ints
    # so lets create a helper property
    @property
    def int_pos(self):
        return map(int, self.pos)

    @property
    def target(self):
        return self.t_x, self.t_y

    @property
    def int_target(self):
        return map(int, self.target)   

    def set_target(self, pos):
        self.t_x, self.t_y = pos
        # print str(t_x) + " " + str(t_y)
        # print "---------------------------"


# pygame.init()
# quit = False
# s = pygame.display.set_mode((300, 300))
# c = pygame.time.Clock()
# ship = Ship()
# 
# while not quit:
#     quit = pygame.event.get(pygame.QUIT)
#     if pygame.event.get(pygame.MOUSEBUTTONDOWN):
#         ship.set_target(pygame.mouse.get_pos())
#     pygame.event.poll()
#     ship.update()
#     s.fill((0, 0, 255))
#     ship.draw(s)
#     pygame.display.flip()
#     c.tick(60)