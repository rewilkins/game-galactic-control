#!/usr/bin/env python

import pygame
from Global_Defs import *
from pygame.color import THECOLORS
from Rectangle import *
from Circle import *
from Ships import *
from starfields import *
from basefields import *
from comp_AI import *
from Instructions import *
import os

import random
import math

class Map(object):
#     pygame.draw.rect(window, silver, (350, 225, 150, 400), 5) # scoreboard boarder

    def run_OutOfBounds_screen(self, window):
        medFont = pygame.font.SysFont("monospace", 44, bold=True)
        label_1 = medFont.render("Warning: Out of Bounds!", 1, red)
        window.blit(label_1, (563, 350))
        pygame.display.update()

    def run_tie_screen(self, window):
        s = pygame.Surface((window_width, window_height), pygame.SRCALPHA) # dims out the game
        s.fill((0, 0, 0, 200))
        window.blit(s, (0, 0))
        
        myfont = pygame.font.SysFont("monospace", 65, bold=True)
        medFont = pygame.font.SysFont("monospace", 44, bold=True)
        smallFont = pygame.font.SysFont("monospace", 24, bold=True)
        
        label_1 = myfont.render("Tie!", 1, red)
        window.blit(label_1, (563, 350))
        label_2 = medFont.render("The game can't progress without ships!", 1, red)
        window.blit(label_2, (563, 425))
        directions = smallFont.render("Press \"ESCAPE\" to return.", 1, red)
        window.blit(directions, (563, 500))
        
        pygame.display.update()
        
        while(True):
            keyState = pygame.key.get_pressed()
            for event in pygame.event.get():
                if (keyState[pygame.K_ESCAPE]):
                    return

    def run_win_screen(self, window):
        s = pygame.Surface((window_width, window_height), pygame.SRCALPHA) # dims out the game
        s.fill((0, 0, 0, 200))
        window.blit(s, (0, 0))
        
        myfont = pygame.font.SysFont("monospace", 65, bold=True)
        smallFont = pygame.font.SysFont("monospace", 24, bold=True)
        
        label = myfont.render("You Won!!!", 1, red)
        window.blit(label, (563, 350))
        directions = smallFont.render("Press \"ESCAPE\" to return.", 1, red)
        window.blit(directions, (563, 450))
        
        pygame.display.update()
        
        while(True):
            keyState = pygame.key.get_pressed()
            for event in pygame.event.get():
                if (keyState[pygame.K_ESCAPE]):
                    return

    def run_lose_screen(self, window):
        s = pygame.Surface((window_width, window_height), pygame.SRCALPHA) # dims out the game
        s.fill((0, 0, 0, 200))
        window.blit(s, (0, 0))
        
        myfont = pygame.font.SysFont("monospace", 65, bold=True)
        smallFont = pygame.font.SysFont("monospace", 24, bold=True)
        
        label = myfont.render("You Lost...", 1, red)
        window.blit(label, (563, 350))
        directions = smallFont.render("Press \"ESCAPE\" to return.", 1, red)
        window.blit(directions, (563, 450))
        
        pygame.display.update()
        
        while(True):
            keyState = pygame.key.get_pressed()
            for event in pygame.event.get():
                if (keyState[pygame.K_ESCAPE]):
                    return
     
    def main(self, window, player_color, enemy_color):
        
        fpsClock = pygame.time.Clock()
        pan_speed = 75
        
        highlight_show = False
        scrolling = False
        
        pos_Start = (0, 0)
        
        paused = False
        
        display_bg_stars = True
        dev_cheat_1 = False
        
        pos_Current = (0, 0)
        
##-----------------------------------------------------------------------------------------------------------------------------------------##
##-----------------------------------------------------------------------------------------------------------------------------------------##

        explode_sound = pygame.mixer.Sound( "./Explosion.ogg" )
        explode_sound.set_volume(1.0)

        myfont = pygame.font.SysFont("monospace", 45, bold=True)
        fps_font = pygame.font.SysFont("monospace", 24, bold=True)

        label1_selected = False
        label2_selected = False
        label3_selected = False
        label4_selected = False
        label5_selected = False
        
        mouse_position = Rectangle(fuchsia, 2, 2)
        mouse_position_circ = Circle((0,0), r=2)
        mouse_position_circ.set_color(player_color)
        
        label1_area = Rectangle(fuchsia, 400, 90, 2)
        label2_area = Rectangle(fuchsia, 300, 90, 2)
        label3_area = Rectangle(fuchsia, 200, 90, 2)
        label4_area = Rectangle(fuchsia, 600, 90, 2)
        label5_area = Rectangle(fuchsia, 600, 90, 2)
        
        label1_area.set_position(400 - 45, 400 - 22)
        label2_area.set_position(400 - 45, 400 + 90 - 22)
        label3_area.set_position(400 - 45, 400 + 90 + 90 - 22)
        label4_area.set_position(400 - 45, 400 + 90 + 90 - 22 + 100)
        label5_area.set_position(400 - 45, 400 + 90 + 90 - 22 + 100 + 90)
        
##-----------------------------------------------------------------------------------------------------------------------------------------##
##-----------------------------------------------------------------------------------------------------------------------------------------##

#       init Galaxy and star-systems
        colors=[THECOLORS["white"],THECOLORS["yellow"], THECOLORS["white"],THECOLORS["red"],THECOLORS["white"]]
        s = Starfields(window_width,window_height)
        b = Basefields(window_width,window_height)
    
        turn = 45.0 * 2 * math.pi / 360.0 #calculate in rad
        deg = 270.0 * 2 * math.pi / 360.0
        
        s.createSpiralGalaxy(window, colors, (window_width/2,window_height/2), (200,200), turn, deg, 75, 16)
        background_galaxy_group = s.background_galaxy_group
        b.createSpiralGalaxy(colors, (window_width/2,window_height/2), (200,200), turn, deg, 37.5, 16)
        background_base_group = b.background_base_group
        
        ######################################################################
        player_ship_group = []
        rand_base1 = random.randrange(0, len(background_base_group))
        ship = Ship((background_base_group[rand_base1].x, background_base_group[rand_base1].y), 5, player_color, 0)
        player_ship_group.append(ship)
        background_base_group[rand_base1].color = player_color
        making_ship = False
        ######################################################################
        enemy_ship_group = []
        rand_base2 = random.randrange(0, len(background_base_group))
        while(rand_base2 == rand_base1):
            rand_base2 = random.randrange(0, len(background_base_group))
        ship = Ship((background_base_group[rand_base2].x, background_base_group[rand_base2].y), 5, enemy_color, 0)
        enemy_ship_group.append(ship)
        background_base_group[rand_base2].color = enemy_color
        enemy_making_ship = False
        AI_delayer = 0
        ######################################################################
##-----------------------------------------------------------------------------------------------------------------------------------------##
##-----------------------------------------------------------------------------------------------------------------------------------------##   
        
#       init background
        background_bg01_group = pygame.sprite.Group()
        for i in range(1, star_density):
            star_color = random.randrange(1, 100)

            if(star_color > 0 and star_color <= line1):
                background_bg01 = Rectangle( white, 2, 2 )
            if(star_color > line1 and star_color <= line2):
                background_bg01 = Rectangle( yellow, 2, 2 )
            if(star_color > line2 and star_color <= line3):
                background_bg01 = Rectangle( royalblue, 3, 3 )
            if(star_color > line3 and star_color <= line4):
                background_bg01 = Rectangle( orangered, 4, 4 )
            background_bg01.set_position(random.randrange(render_zone_left, render_zone_right), random.randrange(render_zone_top, render_zone_bottom))
            background_bg01_group.add( background_bg01 )
        
        background_bg02_group = pygame.sprite.Group()
        for i in range(1, star_density):
            star_color = random.randrange(1, 100)
            background_bg02 = Rectangle( white, 2, 2 )

            if(star_color > 0 and star_color <= line1):
                background_bg02 = Rectangle( white, 2, 2 )
            if(star_color > line1 and star_color <= line2):
                background_bg02 = Rectangle( yellow, 2, 2 )
            if(star_color > line2 and star_color <= line3):
                background_bg02 = Rectangle( royalblue, 3, 3 )
            if(star_color > line3 and star_color <= line4):
                background_bg02 = Rectangle( orangered, 4, 4 )

            background_bg02.set_position(random.randrange(render_zone_left, render_zone_right), random.randrange(render_zone_top, render_zone_bottom))
            background_bg02_group.add( background_bg02 )
        
        background_bg03_group = pygame.sprite.Group()
        for i in range(1, star_density):
            star_color = random.randrange(1, 100)
            background_bg03 = Rectangle( white, 2, 2 )

            if(star_color > 0 and star_color <= line1):
                background_bg03 = Rectangle( white, 2, 2 )
            if(star_color > line1 and star_color <= line2):
                background_bg03 = Rectangle( yellow, 2, 2 )
            if(star_color > line2 and star_color <= line3):
                background_bg03 = Rectangle( royalblue, 3, 3 )
            if(star_color > line3 and star_color <= line4):
                background_bg03 = Rectangle( orangered, 4, 4 )

            background_bg03.set_position(random.randrange(render_zone_left, render_zone_right), random.randrange(render_zone_top, render_zone_bottom))
            background_bg03_group.add( background_bg03 )
        
        background_bg04_group = pygame.sprite.Group()
        for i in range(1, star_density):
            star_color = random.randrange(1, 100)
            background_bg04 = Rectangle( white, 2, 2 )

            if(star_color > 0 and star_color <= line1):
                background_bg04 = Rectangle( white, 2, 2 )
            if(star_color > line1 and star_color <= line2):
                background_bg04 = Rectangle( yellow, 2, 2 )
            if(star_color > line2 and star_color <= line3):
                background_bg04 = Rectangle( royalblue, 3, 3 )
            if(star_color > line3 and star_color <= line4):
                background_bg04 = Rectangle( orangered, 4, 4 )

            background_bg04.set_position(random.randrange(render_zone_left, render_zone_right), random.randrange(render_zone_top, render_zone_bottom))
            background_bg04_group.add( background_bg04 )
        
        background_bg05_group = pygame.sprite.Group()
        for i in range(1, star_density):
            star_color = random.randrange(1, 100)
            background_bg05 = Rectangle( white, 2, 2 )

            if(star_color > 0 and star_color <= line1):
                background_bg05 = Rectangle( white, 2, 2 )
            if(star_color > line1 and star_color <= line2):
                background_bg05 = Rectangle( yellow, 2, 2 )
            if(star_color > line2 and star_color <= line3):
                background_bg05 = Rectangle( royalblue, 3, 3 )
            if(star_color > line3 and star_color <= line4):
                background_bg05 = Rectangle( orangered, 4, 4 )

            background_bg05.set_position(random.randrange(render_zone_left, render_zone_right), random.randrange(render_zone_top, render_zone_bottom))
            background_bg05_group.add( background_bg05 )
        
        background_bg06_group = pygame.sprite.Group()
        for i in range(1, star_density):
            star_color = random.randrange(1, 100)
            background_bg06 = Rectangle( white, 2, 2 )

            if(star_color > 0 and star_color <= line1):
                background_bg06 = Rectangle( white, 2, 2 )
            if(star_color > line1 and star_color <= line2):
                background_bg06 = Rectangle( yellow, 2, 2 )
            if(star_color > line2 and star_color <= line3):
                background_bg06 = Rectangle( royalblue, 3, 3 )
            if(star_color > line3 and star_color <= line4):
                background_bg06 = Rectangle( orangered, 4, 4 )

            background_bg06.set_position(random.randrange(render_zone_left, render_zone_right), random.randrange(render_zone_top, render_zone_bottom))
            background_bg06_group.add( background_bg06 )
            
##-----------------------------------------------------------------------------------------------------------------------------------------##
##-----------------------------------------------------------------------------------------------------------------------------------------##
        
        highlight = Rectangle ( limegreen, 0, 0, 2)
        
##-----------------------------------------------------------------------------------------------------------------------------------------##
##-----------------------------------------------------------------------------------------------------------------------------------------##

        self.bg_sound = pygame.mixer.Sound( "./exit-jump.ogg" )
        # self.bg_sound.set_volume(0.25)
        self.bg_sound.play(loops = 0)
        pygame.time.delay(800)

        window.fill( white )
        pygame.display.update()
        pygame.time.delay(50)
           
           
           
        # finds the outmost regions of the galaxy
        # this is used for marking the map boundaries.
        # go too far off screen and you loose
        north_star = background_galaxy_group[0]
        east_star = background_galaxy_group[0]
        south_star = background_galaxy_group[0]
        west_star = background_galaxy_group[0]
        for s in background_galaxy_group:
            if(s.rect.x < west_star.rect.x): west_star = s
            if(s.rect.x > east_star.rect.x): east_star = s
            if(s.rect.y < north_star.rect.y): north_star = s
            if(s.rect.y > south_star.rect.y): south_star = s
        
#       Game loop
        while(True):
#           variables:
            deltaTime = fpsClock.tick(maxFPS)



#           Event loop
            keyState = pygame.key.get_pressed()
            for event in pygame.event.get():
                if (event.type == pygame.QUIT):
                    return
                
                if (keyState[pygame.K_DELETE]):
                    print "-------------------"
                
                if (event.type == pygame.KEYDOWN):
                    if (event.key == pygame.K_ESCAPE):
                        if (paused == False):
                            paused = True
                        else:
                            paused = False

                if (keyState[pygame.K_w] or keyState[pygame.K_a] or keyState[pygame.K_s] or keyState[pygame.K_d]):
                    scrolling = True
                    highlight_show = False
                    
                if not (keyState[pygame.K_w] or keyState[pygame.K_a] or keyState[pygame.K_s] or keyState[pygame.K_d]):
                    scrolling = False
                
                if (paused == False): 
                    if (event.type == pygame.KEYDOWN):
                        if (event.key == pygame.K_w):
                            for i in background_galaxy_group:
                                i.change_speed( 0, 7)
                            for i in background_base_group:
                                i.change_speed( 0, 7)
                            for i in background_bg06_group:
                                i.change_speed( 0, 6 )
                            for i in background_bg05_group:
                                i.change_speed( 0, 5 )
                            for i in background_bg04_group:
                                i.change_speed( 0, 4 )
                            for i in background_bg03_group:
                                i.change_speed( 0, 3 )
                            for i in background_bg02_group:
                                i.change_speed( 0, 2 )
                            for i in background_bg01_group:
                                i.change_speed( 0, 1 )
                            highlight.change_speed( 0, 7 )
                            for i in player_ship_group:
                                i.change_speed( 0, 7 )
                            for i in enemy_ship_group:
                                i.change_speed( 0, 7 )
    
                        if (event.key == pygame.K_a):
                            for i in background_galaxy_group:
                                i.change_speed( 7, 0)
                            for i in background_base_group:
                                i.change_speed( 7, 0)
                            for i in background_bg06_group:
                                i.change_speed( 6, 0 )
                            for i in background_bg05_group:
                                i.change_speed( 5, 0 )
                            for i in background_bg04_group:
                                i.change_speed( 4, 0 )
                            for i in background_bg03_group:
                                i.change_speed( 3, 0 )
                            for i in background_bg02_group:
                                i.change_speed( 2, 0 )
                            for i in background_bg01_group:
                                i.change_speed( 1, 0 )
                            highlight.change_speed( 7, 0 )
                            for i in player_ship_group:
                                i.change_speed( 7, 0 )
                            for i in enemy_ship_group:
                                i.change_speed( 7, 0 )
    
                        if (event.key == pygame.K_s):
                            for i in background_galaxy_group:
                                i.change_speed( 0, -7)
                            for i in background_base_group:
                                i.change_speed( 0, -7)
                            for i in background_bg06_group:
                                i.change_speed( 0, -6 )
                            for i in background_bg05_group:
                                i.change_speed( 0, -5 )
                            for i in background_bg04_group:
                                i.change_speed( 0, -4 )
                            for i in background_bg03_group:
                                i.change_speed( 0, -3 )
                            for i in background_bg02_group:
                                i.change_speed( 0, -2 )
                            for i in background_bg01_group:
                                i.change_speed( 0, -1 )
                            highlight.change_speed( 0, -7 )
                            for i in player_ship_group:
                                i.change_speed( 0, -7 )
                            for i in enemy_ship_group:
                                i.change_speed( 0, -7 )
                            
                        if (event.key == pygame.K_d):
                            for i in background_galaxy_group:
                                i.change_speed( -7, 0)
                            for i in background_base_group:
                                i.change_speed( -7, 0)
                            for i in background_bg06_group:
                                i.change_speed( -6, 0 )
                            for i in background_bg05_group:
                                i.change_speed( -5, 0 )
                            for i in background_bg04_group:
                                i.change_speed( -4, 0 )
                            for i in background_bg03_group:
                                i.change_speed( -3, 0 )
                            for i in background_bg02_group:
                                i.change_speed( -2, 0 )
                            for i in background_bg01_group:
                                i.change_speed( -1, 0 )
                            highlight.change_speed( -7, 0 )
                            for i in player_ship_group:
                                i.change_speed( -7, 0 )
                            for i in enemy_ship_group:
                                i.change_speed( -7, 0 )
                
                if (event.type == pygame.KEYUP):
                    if (event.key == pygame.K_w ):
                        for i in background_galaxy_group:
                            i.change_speed( 0, -7)
                        for i in background_base_group:
                            i.change_speed( 0, -7)
                        for i in background_bg06_group:
                            i.change_speed( 0, -6 )
                        for i in background_bg05_group:
                            i.change_speed( 0, -5 )
                        for i in background_bg04_group:
                            i.change_speed( 0, -4 )
                        for i in background_bg03_group:
                            i.change_speed( 0, -3 )
                        for i in background_bg02_group:
                            i.change_speed( 0, -2 )
                        for i in background_bg01_group:
                            i.change_speed( 0, -1 )
                        highlight.change_speed( 0, -7 )
                        for i in player_ship_group:
                            i.change_speed( 0, -7 )
                        for i in enemy_ship_group:
                            i.change_speed( 0, -7 )
                        
                    if (event.key == pygame.K_a ):
                        for i in background_galaxy_group:
                            i.change_speed( -7, 0)
                        for i in background_base_group:
                            i.change_speed( -7, 0)
                        for i in background_bg06_group:
                            i.change_speed( -6, 0 )
                        for i in background_bg05_group:
                            i.change_speed( -5, 0 )
                        for i in background_bg04_group:
                            i.change_speed( -4, 0 )
                        for i in background_bg03_group:
                            i.change_speed( -3, 0 )
                        for i in background_bg02_group:
                            i.change_speed( -2, 0 )
                        for i in background_bg01_group:
                            i.change_speed( -1, 0 )
                        highlight.change_speed( -7, 0 )
                        for i in player_ship_group:
                            i.change_speed( -7, 0 )
                        for i in enemy_ship_group:
                            i.change_speed( -7, 0 )
                        
                    if (event.key == pygame.K_s ):
                        for i in background_galaxy_group:
                            i.change_speed( 0, 7)
                        for i in background_base_group:
                            i.change_speed( 0, 7)
                        for i in background_bg06_group:
                            i.change_speed( 0, 6 )
                        for i in background_bg05_group:
                            i.change_speed( 0, 5 )
                        for i in background_bg04_group:
                            i.change_speed( 0, 4 )
                        for i in background_bg03_group:
                            i.change_speed( 0, 3 )
                        for i in background_bg02_group:
                            i.change_speed( 0, 2 )
                        for i in background_bg01_group:
                            i.change_speed( 0, 1 )
                        highlight.change_speed( 0, 7 )
                        for i in player_ship_group:
                            i.change_speed( 0, 7 )
                        for i in enemy_ship_group:
                            i.change_speed( 0, 7 )
                        
                    if (event.key == pygame.K_d ):
                            for i in background_galaxy_group:
                                i.change_speed( 7, 0)
                            for i in background_base_group:
                                i.change_speed( 7, 0)
                            for i in background_bg06_group:
                                i.change_speed( 6, 0 )
                            for i in background_bg05_group:
                                i.change_speed( 5, 0 )
                            for i in background_bg04_group:
                                i.change_speed( 4, 0 )
                            for i in background_bg03_group:
                                i.change_speed( 3, 0 )
                            for i in background_bg02_group:
                                i.change_speed( 2, 0 )
                            for i in background_bg01_group:
                                i.change_speed( 1, 0 )
                            highlight.change_speed( 7, 0 )
                            for i in player_ship_group:
                                i.change_speed( 7, 0 )
                            for i in enemy_ship_group:
                                i.change_speed( 7, 0 )
                    
                if(scrolling == False):
                    if (event.type == pygame.MOUSEBUTTONDOWN and event.button == RIGHT_):
                        pos_Start = pygame.mouse.get_pos()
                        highlight_show = False
                        
                        # enemy_ship_group[0].set_target(pygame.mouse.get_pos())
                        
                        for i in player_ship_group:
                            if(i.selected == True):
                                i.set_target(pygame.mouse.get_pos())
                                i.moving = True
                    
                    if (event.type == pygame.MOUSEBUTTONDOWN and event.button == LEFT_):
                        pos_Start = pygame.mouse.get_pos()
                        highlight.set_position( (pos_Start[X_]), (pos_Start[Y_]) )
                        highlight_show = True
                        
                        if(paused == True):
                            if(label1_selected == True):
                                Instruction_loop(window)
                            if(label2_selected == True):
                                paused = False
                            if(label3_selected == True):
                                return
                            if(label4_selected == True):
                                display_bg_stars = not display_bg_stars
                            if(label5_selected == True):
                                dev_cheat_1 = not dev_cheat_1
                    
                    if (event.type == pygame.MOUSEBUTTONUP and event.button == LEFT_):
                        pos_End = pygame.mouse.get_pos()
                        
                        if(highlight.width < 10 or highlight.height < 10):
                            for i in player_ship_group:
                                i.selected = False
                                if(i.getDist(pos_Current[X_], pos_Current[Y_]) < 10):
                                    i.selected = True
                                    
                        highlight.set_dimensions(0, 0)
                        highlight_show = False
  
                    if not (keyState[pygame.K_ESCAPE]):
                        if (event.type == pygame.MOUSEMOTION):
                            
                            pos_Current = pygame.mouse.get_pos()
                            if(highlight_show == True): highlight.drag_to_set_dimensions(pos_Start, pos_Current)
                            
                            # drag to highlight
                            if(highlight_show == True):
                                for i in player_ship_group:
                                    i.selected = False
                                    if(i.collisionDetection_rect(highlight)):
                                        i.selected = True
                            
                            # print "1"
                            if(mouse_position.collisionDetection(label1_area)):
                                label1_selected = True
                            else:
                                label1_selected = False
                            # print "2"
                            if(mouse_position.collisionDetection(label2_area)):
                                label2_selected = True
                            else:
                                label2_selected = False
                            # print "3"
                            if(mouse_position.collisionDetection(label3_area)):
                                label3_selected = True
                            else:
                                label3_selected = False
                            if(mouse_position.collisionDetection(label4_area)):
                                label4_selected = True
                            else:
                                label4_selected = False
                            if(mouse_position.collisionDetection(label5_area)):
                                label5_selected = True
                            else:
                                label5_selected = False
                            
                            if(dev_cheat_1 == True):
                                for i in background_base_group:
                                    if(mouse_position_circ.collisionDetection(i)):
                                        i.set_color(player_color)

#           Update Phase

#           wrap stars around the screen when they go offscreen
            for i in background_bg06_group:
                if(i.rect.x < render_zone_left):
                    i.set_position( i.rect.x + render_zone_right, i.rect.y )
                if(i.rect.x > render_zone_right):
                    i.set_position( i.rect.x - render_zone_right, i.rect.y )
                if(i.rect.y < render_zone_top):
                    i.set_position( i.rect.x, i.rect.y + render_zone_bottom )
                if(i.rect.y > render_zone_bottom):
                    i.set_position( i.rect.x, i.rect.y - render_zone_bottom )
                    
            for i in background_bg05_group:
                if(i.rect.x < render_zone_left):
                    i.set_position( i.rect.x + render_zone_right, i.rect.y )
                if(i.rect.x > render_zone_right):
                    i.set_position( i.rect.x - render_zone_right, i.rect.y )
                if(i.rect.y < render_zone_top):
                    i.set_position( i.rect.x, i.rect.y + render_zone_bottom )
                if(i.rect.y > render_zone_bottom):
                    i.set_position( i.rect.x, i.rect.y - render_zone_bottom )
                    
            for i in background_bg04_group:
                if(i.rect.x < render_zone_left):
                    i.set_position( i.rect.x + render_zone_right, i.rect.y )
                if(i.rect.x > render_zone_right):
                    i.set_position( i.rect.x - render_zone_right, i.rect.y )
                if(i.rect.y < render_zone_top):
                    i.set_position( i.rect.x, i.rect.y + render_zone_bottom )
                if(i.rect.y > render_zone_bottom):
                    i.set_position( i.rect.x, i.rect.y - render_zone_bottom )
                    
            for i in background_bg03_group:
                if(i.rect.x < render_zone_left):
                    i.set_position( i.rect.x + render_zone_right, i.rect.y )
                if(i.rect.x > render_zone_right):
                    i.set_position( i.rect.x - render_zone_right, i.rect.y )
                if(i.rect.y < render_zone_top):
                    i.set_position( i.rect.x, i.rect.y + render_zone_bottom )
                if(i.rect.y > render_zone_bottom):
                    i.set_position( i.rect.x, i.rect.y - render_zone_bottom )
                    
            for i in background_bg02_group:
                if(i.rect.x < render_zone_left):
                    i.set_position( i.rect.x + render_zone_right, i.rect.y )
                if(i.rect.x > render_zone_right):
                    i.set_position( i.rect.x - render_zone_right, i.rect.y )
                if(i.rect.y < render_zone_top):
                    i.set_position( i.rect.x, i.rect.y + render_zone_bottom )
                if(i.rect.y > render_zone_bottom):
                    i.set_position( i.rect.x, i.rect.y - render_zone_bottom )
                    
            for i in background_bg01_group:
                if(i.rect.x < render_zone_left):
                    i.set_position( i.rect.x + render_zone_right, i.rect.y )
                if(i.rect.x > render_zone_right):
                    i.set_position( i.rect.x - render_zone_right, i.rect.y )
                if(i.rect.y < render_zone_top):
                    i.set_position( i.rect.x, i.rect.y + render_zone_bottom )
                if(i.rect.y > render_zone_bottom):
                    i.set_position( i.rect.x, i.rect.y - render_zone_bottom )
            
            
            # get base counts
            player_count = 0
            enemy_count = 0
            nutral_count = 0
            for i in background_base_group:
                if(i.color == player_color):
                    player_count = player_count + 1
                if(i.color == enemy_color):
                    enemy_count = enemy_count + 1
                if(i.color == whitesmoke):
                    nutral_count = nutral_count + 1
            label_player_base_count = myfont.render(str(player_count), 1, player_color)
            label_enemy_base_count = myfont.render(str(enemy_count), 1, enemy_color)
            label_gray_base_count = myfont.render(str(nutral_count), 1, gray)
            
            # get ship counts
            player_ship_count = len(player_ship_group)
            enemy_ship_count = len(enemy_ship_group)
            label_player_ship_count = fps_font.render("Ships: " + str(player_ship_count), 1, player_color)
            label_enemy_ship_count = fps_font.render("Ships: " + str(enemy_ship_count), 1, enemy_color)
            
            if(paused == False):
                # run AI for computer player
                AI_delayer = AI_delayer + 1
                if(AI_delayer > int(fpsClock.get_fps()*2)):  # delay movement by 1 sec after base capture
                    AI_delayer = 0
                    for e in enemy_ship_group:
                        base_target = run_AI(e, background_base_group, player_color, enemy_color)
                        e.set_target((background_base_group[base_target].x, background_base_group[base_target].y))
            
                # check for attacks
                for p in player_ship_group:
                    removed = False
                    for e in enemy_ship_group:
                        if(p.collisionDetection(e) == True):
                            if(removed == False):
                                removed = True
                                explode_sound.play()
                                player_ship_group.remove(p)
                                enemy_ship_group.remove(e)
    
                # checks for base captures
                making_ship = False
                if(len(player_ship_group) > 0):
                    for b in background_base_group:
                        for p in player_ship_group:
                            if(b.collisionDetection(p) and b.color != player_color):
                                temp3 = True
                                for e in enemy_ship_group:
                                    if(e.collisionDetection(b) == True):
                                        temp3 = False
                                if(temp3 == True):
                                    if(len(player_ship_group) <= player_count): # can only have as many ships as bases
                                        making_ship = True
                                        ship = Ship((b.x, b.y), 5, player_color, 0)
                                        player_ship_group.append(ship)
                                    b.set_color(player_color)
                
                enemy_making_ship = False
                if(len(enemy_ship_group) > 0):
                    for b in background_base_group:
                        for i in enemy_ship_group:
                            if(b.collisionDetection(i) and b.color != enemy_color):
                                temp3 = True
                                for p in player_ship_group:
                                    if(p.collisionDetection(b) == True):
                                        temp3 = False
                                if(temp3 == True):
                                    if(len(enemy_ship_group) <= enemy_count): # can only have as many ships as bases
                                        enemy_making_ship = True
                                        ship = Ship((b.x, b.y), 5, enemy_color, 0)
                                        enemy_ship_group.append(ship)
                                    b.set_color(enemy_color)
                        
            # ships fly off screen on creation patch
            if(making_ship == True):
                if(keyState[pygame.K_w]):
                    player_ship_group[len(player_ship_group) - 1].change_speed( 0, 7 )
                if(keyState[pygame.K_a]):
                    player_ship_group[len(player_ship_group) - 1].change_speed( 7, 0 )
                if(keyState[pygame.K_s]):
                    player_ship_group[len(player_ship_group) - 1].change_speed( 0, -7 )
                if(keyState[pygame.K_d]):
                    player_ship_group[len(player_ship_group) - 1].change_speed( -7, 0 )
            for p in player_ship_group:
                if(p.x < window_width*-3):
                    player_ship_group.remove(p)
                if(p.x > window_width*3):
                    player_ship_group.remove(p)
                if(p.y < window_height*-3):
                    player_ship_group.remove(p)
                if(p.y > window_height*3):
                    player_ship_group.remove(p)
            for e in enemy_ship_group:
                if(e.x < window_width*-3):
                    enemy_ship_group.remove(e)
                if(e.x > window_width*3):
                    enemy_ship_group.remove(e)
                if(e.y < window_height*-3):
                    enemy_ship_group.remove(e)
                if(e.y > window_height*3):
                    enemy_ship_group.remove(e)
            # end patch
                    
            if(enemy_making_ship == True):
                if(keyState[pygame.K_w]):
                    enemy_ship_group[len(enemy_ship_group) - 1].change_speed( 0, 7 )
                if(keyState[pygame.K_a]):
                    enemy_ship_group[len(enemy_ship_group) - 1].change_speed( 7, 0 )
                if(keyState[pygame.K_s]):
                    enemy_ship_group[len(enemy_ship_group) - 1].change_speed( 0, -7 )
                if(keyState[pygame.K_d]):
                    enemy_ship_group[len(enemy_ship_group) - 1].change_speed( -7, 0 )
            
            # force any non-moving ship to not stop on each other
            if(len(player_ship_group) > 1):
                temp1 = 0
                for i in player_ship_group:
                    temp1 = temp1 + 1
                    temp2 = 0
                    for j in player_ship_group:
                        temp2 = temp2 + 1
                        if(temp1 != temp2): # prevents checking collision against itself
                            if(i.dist < 2 and j.dist < 2):
                                if (i.collisionDetection(j)):
                                    if(i.moved == False):
                                        range1 = random.randrange(0, 4)
                                        if(range1 == 0):
                                            i.set_position(i.x + 1, i.y + 1)
                                        if(range1 == 1):
                                            i.set_position(i.x + 1, i.y - 1)
                                        if(range1 == 2):
                                            i.set_position(i.x - 1, i.y - 1)
                                        if(range1 == 3):
                                            i.set_position(i.x - 1, i.y + 1)
                                        i.moved = True
                                        
            if(len(enemy_ship_group) > 1):
                temp1 = 0
                for i in enemy_ship_group:
                    temp1 = temp1 + 1
                    temp2 = 0
                    for j in enemy_ship_group:
                        temp2 = temp2 + 1
                        if(temp1 != temp2): # prevents checking collision against itself
                            if(i.dist < 2 and j.dist < 2):
                                if (i.collisionDetection(j)):
                                    if(i.moved == False):
                                        range1 = random.randrange(0, 4)
                                        if(range1 == 0):
                                            i.set_position(i.x + 1, i.y + 1)
                                        if(range1 == 1):
                                            i.set_position(i.x + 1, i.y - 1)
                                        if(range1 == 2):
                                            i.set_position(i.x - 1, i.y - 1)
                                        if(range1 == 3):
                                            i.set_position(i.x - 1, i.y + 1)
                                        i.moved = True
        
        
            if (paused == False):
                for i in player_ship_group:
                    i.update()
                for i in enemy_ship_group:
                    i.update()
                
                for i in background_base_group:
                    i.update()
                for i in background_galaxy_group:
                    i.update()
                if (display_bg_stars == True):
                    background_bg06_group.update()
                    background_bg05_group.update()
                    background_bg04_group.update()
                    background_bg03_group.update()
                    background_bg02_group.update()
                    background_bg01_group.update()
                highlight.update()

            mouse_pos = pygame.mouse.get_pos()
            mouse_position.set_position(mouse_pos[X_], mouse_pos[Y_])
            mouse_position_circ.set_position(mouse_pos[X_], mouse_pos[Y_])

            if(paused == True):
                if(label1_selected == False):
                    label1 = myfont.render("Instructions", 1, yellowgreen)
                else:
                    label1 = myfont.render("Instructions", 1, orange)
                if(label2_selected == False):
                    label2 = myfont.render("Continue", 1, yellowgreen)
                else:
                    label2 = myfont.render("Continue", 1, orange)
                if(label3_selected == False):
                    label3 = myfont.render("Quit", 1, yellowgreen)
                else:
                    label3 = myfont.render("Quit", 1, orange)
                    
                if(display_bg_stars == True):
                    if(label4_selected == False):
                        label4 = myfont.render("Background Stars <On>", 1, yellowgreen)
                    else:
                        label4 = myfont.render("Background Stars <On>", 1, orange)
                else:
                    if(label4_selected == False):
                        label4 = myfont.render("Background Stars <Off>", 1, yellowgreen)
                    else:
                        label4 = myfont.render("Background Stars <Off>", 1, orange)
                
                if(dev_cheat_1 == True):
                    if(label5_selected == False):
                        label5 = myfont.render("Click to Capture <On>", 1, yellowgreen)
                    else:
                        label5 = myfont.render("Click to Capture <On>", 1, orange)
                else:
                    if(label5_selected == False):
                        label5 = myfont.render("Click to Capture <Off>", 1, yellowgreen)
                    else:
                        label5 = myfont.render("Click to Capture <Off>", 1, orange)
            
            label_fps = fps_font.render("FPS: " + str(int(fpsClock.get_fps())), 1, red)
            
#           Draw Phase
            window.fill( black )
            
            if (paused == False and display_bg_stars == True):
                background_bg01_group.draw( window )
                background_bg02_group.draw( window )
                background_bg03_group.draw( window )
                background_bg04_group.draw( window )
                background_bg05_group.draw( window )
                background_bg06_group.draw( window )
            
            # always display the galaxy even when the game is paused    
            for i in background_galaxy_group:
                i.display(window)
                
            if (paused == False):
                for i in background_base_group:
                    i.display(window)
                for i in enemy_ship_group:
                    i.display(window)
                for i in player_ship_group:
                    i.display(window)
                
            
            if(highlight_show == True and paused == False): highlight.display(window)
            
            if (paused == False):
                s = pygame.Surface((150, 400), pygame.SRCALPHA) # scoreboard background
                s.fill((0, 0, 0, 175))
                window.blit(s, (350, 225))
                pygame.draw.rect(window, silver, (350, 225, 150, 400), 5) # scoreboard boarder
                window.blit(label_player_base_count, (363, 350))
                window.blit(label_player_ship_count, (363, 390))
                window.blit(label_enemy_base_count, (363, 450))
                window.blit(label_enemy_ship_count, (363, 490))
                window.blit(label_gray_base_count, (363, 550))
            window.blit(label_fps, (363, 250))
            
            if(paused == True):
                window.blit(label1, (400,400))
                window.blit(label2, (400,400 + 90))
                window.blit(label3, (400,400 + 90 + 90))
                window.blit(label4, (400,400 + 90 + 90 + 100))
                window.blit(label5, (400,400 + 90 + 90 + 100 + 90))
            
                # mouse_position.display(window)
                # label1_area.display(window)
                # label2_area.display(window)
                # label3_area.display(window)
                # label4_area.display(window)
                # label5_area.display(window)
            # mouse_position_circ.display(window)
            
            pygame.display.update() # update("empty") = means that it will update everything



#           check for win/lose:
            win = False
            lose = False
            if(player_count == 0 or player_ship_count == 0):
                lose = True
            if(enemy_count == 0 or enemy_ship_count == 0):
                win = True
            if(player_ship_count == 0 and enemy_ship_count == 0):
                self.run_tie_screen(window)
                return
            if(win == True):
                self.run_win_screen(window)
                return
            if(lose == True):
                self.run_lose_screen(window)
                return
            
            #lose by going out of bounds
            out_of_bounds_w = False
            out_of_bounds_a = False
            out_of_bounds_s = False
            out_of_bounds_d = False
            if(west_star.rect.x > window_width):
                out_of_bounds_a = True
            if(east_star.rect.x < 0):
                out_of_bounds_d = True
            if(north_star.rect.y > window_height):
                out_of_bounds_w = True
            if(south_star.rect.y < 0):
                out_of_bounds_s = True
            
            # print warning!
            if(out_of_bounds_w == True or out_of_bounds_a == True or out_of_bounds_s == True or out_of_bounds_d == True):
                self.run_OutOfBounds_screen(window)
                
            # destroy diserter!
            if(west_star.rect.x > window_width*2):
                self.run_lose_screen(window)
                return
            if(east_star.rect.x < window_width*-2):
                self.run_lose_screen(window)
                return
            if(north_star.rect.y > window_height*2):
                self.run_lose_screen(window)
                return
            if(south_star.rect.y < window_height*-2):
                self.run_lose_screen(window)
                return